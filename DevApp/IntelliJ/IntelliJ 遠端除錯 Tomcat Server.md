IntelliJ 遠端除錯 Tomcat Server
======

Tomcat 設定
------

### Win
#### Tomcat/bin/catalina.bat
        set JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,address=8000,suspend=n,server=y
### Mac
#### Tomcat/bin/catalina.sh
        export JAVA_OPTS='-agentlib:jdwp=transport=dt_socket,address=8000,suspend=n,server=y'


IntelliJ 設定
------
Edit Configurations > + > Tomcat Server > Remote 

### Server (Tab)
    Remote Connection Settings:
        Host: 192.168.2.3 (Remote)
        Port: 8080 (Remote)

### Startup/Connection
    Debug:
        Transport: Socket
        Port : 8000
        


參考資料：

<https://www.itread01.com/content/1545280339.html>

<https://www.itread01.com/p/827639.html>