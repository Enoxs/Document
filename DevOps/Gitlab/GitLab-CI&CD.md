Gitlab - CI&CD
======

環境
------
+ Gitlab - 11.10.4
    + <https://packages.gitlab.com/gitlab/gitlab-ce>
+ Gitlab Runner - 12.0.2 
    + <https://docs.gitlab.com/runner/install/windows.html>

安裝
------
### 使用指令方式安裝並啟動服務
    ./gitlab-runner.exe install
    ./gitlab-runner.exe start
    ./gitlab-runner status

![gitlab-runner](assets/GitLab-CI&CD/gitlab-runner.png) 

註冊
------
### 專案資訊
    
![gitlab-project-register](assets/GitLab-CI&CD/gitlab-project-register.png)
![gitlab-project-register](assets/GitLab-CI&CD/gitlab-project-register-01.png)
    
### 執行器註冊

    gitlab-runner register

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
        
    192.168.2.3
    
Please enter the gitlab-ci token for this runner
    
    KwxyFrAhhcb518Q1n49r

Please enter the gitlab-ci description for this runner
        
    Project-CI Test

Please enter the gitlab-ci tags for this runner (comma separated):
my-tag,another-tag

    deploy,release    

Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
docker

    shell
    
### Example                                              
![command-line](assets/GitLab-CI&CD/command-line.png)

### 註冊成功                                              
![register-success](assets/GitLab-CI&CD/register-success.png)

### 執行器觸發設定，第三點不勾                                             
![runner-settings](assets/GitLab-CI&CD/runner-settings.png)

撰寫腳本
------

### 於專案根目錄下創建 .gitlab-ci.yml 檔案

![gitlab-ci-yml](assets/GitLab-CI&CD/gitlab-ci-yml.png)

### .gitlab-ci.yml 檔案內容範例
```
stages:
- build
- test
- deploy
# 打包
build:
  stage: build
  script: 
  - mvn package
# 測試
test:
  stage: test
  script:
  - echo "Unit Test"
  - echo "Check Config"
# 部署
deploy:
  stage: deploy
  script:
  - chcp 65001
  - echo "run deply"
  - deploy/release/apply.sh # 覆蓋測試機環境設定  
  - mvn tomcat7:undeploy
  - mvn tomcat7:deploy
  only:
    - master
# when: manual # 手動
```

### tomcat7-maven-plugin
    mvn tomcat7:undeploy
    mvn tomcat7:deploy
    
此部分有使用到 Maven Tomcat 遠端部署 Plugin 參閱：

<http://tomcat7-maven-plugin.md>

### 腳本完成後，提交推送(commit & push)

Gitlab-Runner 運行結果
------
![pipelines](assets/GitLab-CI&CD/pipelines.png)

Reference
------
Install GitLab Runner - <https://docs.gitlab.com/runner/install/>

Docker搭建自己的Gitlab CI Runner - 
<https://www.itread01.com/content/1546542748.html>

+ [Docker Maven Image](https://hub.docker.com/_/maven?tab=description)
+ [Docker Maven Github](https://github.com/docker-library/repo-info/blob/master/repos/maven/remote/3-jdk-8.md)
          
    
gitlab-runner Docker安装 - 
<https://blog.csdn.net/why_still_confused/article/details/89243006>

gitlab 的 CI/CD 配置管理 （二）-
<https://blog.51cto.com/flyfish225/2156602>

Tomcat + Gitlab CI 配置持續整合環境
<https://janelin612.github.io/2017/08/03/tomcat-with-gitlab-ci.html>
