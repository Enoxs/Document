GitLab-Runner
======

    ./gitlab-runner.exe install
    ./gitlab-runner.exe start
    ./gitlab-runner register

Register
------

```
sudo gitlab-runner register
```
1. Enter your GitLab instance URL:

```
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
https://gitlab.com
```

2. Enter the token you obtained to register the Runner:
```
Please enter the gitlab-ci token for this runner
token-code
```
3. Enter a description for the Runner, you can change this later in GitLab’s UI:
```
Please enter the gitlab-ci description for this runner
[hostname] my-runner
```
4. Enter the tags associated with the Runner, you can change this later in GitLab’s UI:
```
Please enter the gitlab-ci tags for this runner (comma separated):
my-tag,another-tag
```
5. Enter the Runner executor:
```
Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
docker
```
6. If you chose Docker as your executor, you’ll be asked for the default image to be used for projects that do not define one in .gitlab-ci.yml:
```
Please enter the Docker image (eg. ruby:2.1):
alpine:latest
```

指令
------

### unregister
```
gitlab-runner unregister --url https://gitlab.com/ --token Jx6NdRfzHP9Pmk7c5c4s
```
.\gitlab-runner unregister --url http://172.24.80.35/ --token fBMEwV3UUosWhoT4zLYH
Docker
------

```
docker run -d --name gitlab-runner --restart always \
--link gitlab \ 
-v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
-v /var/run/docker.sock:/var/run/docker.sock \
gitlab/gitlab-runner:latest
  
docker run -d --name gitlab-runner --restart always \
-v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
-v /var/run/docker.sock:/var/run/docker.sock \
gitlab/gitlab-runner:latest
```


安裝
------

Download
```
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
```
Permission
```
sudo chmod +x /usr/local/bin/gitlab-runner
```

設定檔
------
### config.toml

```
[[runners]]
  name = "Gitlab-runner"
  url = "https://gitlab.com/"
  token = "zp4QtRbScxCcWSwQUwog"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "maven:3-jdk-8"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache","/Users/Enoxs/.m2:/root/.m2"]
  	pull_policy = "if-not-present"
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

### .gitlab-ci.yml  

```
#image: maven:latest
image: maven:3-jdk-8
stages:
- build
- package
- deploy
build:
  stage: build
  script:
  - echo "開始編譯"
  - mvn compile
# 打包
package:
  stage: package
  script:
  - echo "開始打包"
  - mvn package #-Dmaven.test.skip=true
# 部署
deploy:
  stage: deploy
  script:
  - echo "遠端部署"
  - scp target/Root.war Enoxs@192.168.2.25:/Users/Enoxs/Downloads/
```

Reference
------
Install GitLab Runner
<https://docs.gitlab.com/runner/install/>




