# GitLab 參考資料


GitLab 官方文檔
<https://docs.gitlab.com/>

GitLab-Runner Install
<https://docs.gitlab.com/runner/install/>

<https://lusyoe.github.io/2016/08/29/Gitlab-CI-Multi-Runner搭建CI持续集成环境>
<https://www.jianshu.com/p/68fa788c2fb3>
<https://xingdi.me/archives/16.html>

Docker 教程

<https://ithelp.ithome.com.tw/users/20103456/ironman/1320>

#### Enoxs - GitLab
<http://59.115.133.186:8080/Enoxs/junitse-ci/pipelines>


#### Gitlab - 502 Error
<https://www.cnblogs.com/linkenpark/p/8405327.html>

### ISSUE

+ 使用 Gitlab issue board 達成任務與工作流程視覺化，並搭配 Webhook 打造自動化 Kanban
    + <https://medium.com/sparkamplab/%E4%BD%BF%E7%94%A8-gitlab-issue-board-%E9%81%94%E6%88%90%E4%BB%BB%E5%8B%99%E8%88%87%E5%B7%A5%E4%BD%9C%E6%B5%81%E7%A8%8B%E8%A6%96%E8%A6%BA%E5%8C%96-%E4%B8%A6%E6%90%AD%E9%85%8D-webhook-%E6%89%93%E9%80%A0%E8%87%AA%E5%8B%95%E5%8C%96-kanban-5a05eab8603a>
    
+ 基於GitLab的工作流程設計
    + <https://zhuanlan.zhihu.com/p/38774185>
    
+ 基于 GitLab 的简单项目管理与协作流程
    + <https://www.zlovezl.cn/articles/project-manage-with-gitlab/>
    
+ 如何使用 Issue 管理软件项目？
    + <http://www.ruanyifeng.com/blog/2017/08/issue.html>
    
+ Webhooks 实现自动化服务器项目部署
    + <https://learnku.com/articles/5012/gitlab-webhooks-implements-automated-server-project-deployment> 